﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace RegexE
{
    class Program
    {
        static void Main(string[] args)
        {


            Console.WriteLine($"type in a note with emails inside");
            var note = Console.ReadLine();

            EmailFinder(note);

            Console.ReadLine();
          
        }


        public static void EmailFinder(string note)
        {

            var noteToArray = note.Split();
            var count = 0;
            var allEmails = new List<string>();

            foreach(var item in noteToArray)
            {
               if(isValidEmail(item))
                {
                    count++;
                    allEmails.Add(item);
                }
            }

            Console.WriteLine($"{count} Emails Found: \n");

            foreach (var email in allEmails )
            {
                Console.WriteLine($"{email}");
            }
        }

        public static bool isValidEmail(string inputEmail)
        {

            // This Pattern is use to verify the email
            string strRegex = @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z";

            Regex re = new Regex(strRegex, RegexOptions.IgnoreCase);

            if (re.IsMatch(inputEmail))
                return (true);
            else
                return (false);
        }


    }
    
}
